"use strict";

let enableTests = false;

let cl   = (m) => {console.log(m)};
let cw   = (m) => {console.warn(m)};
let ce   = (m) => {console.error(m)};
let cg   = (l) => {console.group(l)};
let cG   = ( ) => {console.groupEnd()};

let test = (c,ok,ko) => {(c)?cl(ok):ce(ko);}

let globalEventCounter = 0, globalCounter = 0;

document.addEventListener( "JelpEvent", ()=>{globalEventCounter++;} );

let functionArrayNames = [
	"Checking dependencies",
	"Check Constructor( String )",
	"Check Constructor( HTMLElement )",
	"Check Constructor( Jelp )",
	"Check Constructor( Mixed )",
	"Create New HTML Element",
	"Remove One or More HTML Element",
	"Jelp Events",
	"",
];

let functionArray = [
	function(){
		cw( "Check if dependencies are loaded correctly." );
		if(
			typeof _ === "function" &&
			typeof _() === "object" &&
			_().toString() === "Jelp"
		){
			cl( "Jelp is loaded correctly." );
		}
		else{
			ce( "Jelp NOT LOADED, quitting tests." );
			clearInterval( interval );
		}
	},
	function(){
		cw( "Function: Jelp.constructor( String selector, ... )" );
		cw( "Try selector 'input'" );
		let a = _( "input" ) 				// Jelp query selector all
		let b = document.querySelectorAll( "input" ); 	// JS query selector all
		cw( 'a = _( "input" )' )
		cl( a );
		cw( 'b = document.querySelectorAll( "input" )' )
		cl( b );
		test(
			( a.length === b.length ),
			"Objects have same length",
			"Objects not have same length"
		);
		b.forEach( function( v, i ){
			test(
				( v === a[i] ),
				"--> a["+i+"] and b["+i+"] are same Object.",
				"--> a["+i+"] and b["+i+"] aren't same Object."
			);
		} );
	},
	function(){
		cw( "Function: Jelp.constructor( HTMLElement element, ... )" );
		cw( "Try to get element with id 'b1'" );
		let a = _( document.getElementById( "b1" ) ); 	// Jelp element
		let b = document.getElementById( "b1" ); 	// JS get element
		cw( 'a = _( document.getElementById( "b1" ) )' )
		cl( a );
		cw( 'b = document.getElementById( "b1" );' )
		cl( b );
		cw( "Testing if HTMLElement objects are totally equals" );
		test(
			( a[0] === b ),
			"--> a[0] and b are same Object.",
			"--> a[0] and b aren't same Object."
		);
	},
	function(){
		cw( "Function: Jelp.constructor( Jelp jelpObject, ... )" );
		cw( "Try selector 'input'" );
		let a = _( "input" ); 				// Jelp query selector all
		let b = document.querySelectorAll( "input" ); 	// JS query selector all
		let c = _( _( "input" ) );			// Jelp Object
		cw( 'a = _( "input" )' );
		cl( a );
		cw( 'b = _( a )' )
		cl( b );
		cw( 'c = _( _( "input" ) )' )
		cl( c );
		test(
			( a.length === b.length && b.length === c.length ),
			"Objects have same length",
			"Objects not have same length"
		);
		b.forEach( function( v, i ){
			test(
				( v === a[i] && v === c[i] ),
				"--> a["+i+"] and b["+i+"] and c["+i+"] are same Object.",
				"--> a["+i+"] and b["+i+"] and c["+i+"] aren't same Object."
			);
		} );
	},
	function(){
		cw( "Function: Jelp.constructor( ... )" );
		cw( "Try multiple types of selectors '#b1,#b2,#b3'" );
		let a = _( "#b1", document.getElementById("b2"), _( "#b3" ) ); 	// Jelp mixed content
		let b = document.querySelectorAll( "#b1,#b2,#b3" ); 		// JS query selector all
		cw( 'a = _( "#b1", document.getElementById("b2"), _( "#b3" ) )' )
		cl( a );
		cw( 'b = document.querySelectorAll( "#b1,#b2,#b3" )' )
		cl( b );
		test(
			( a.length === b.length ),
			"Objects have same length",
			"Objects not have same length"
		);
		b.forEach( function( v, i ){
			test(
				( v === a[i] ),
				"--> a["+i+"] and b["+i+"] are same Object.",
				"--> a["+i+"] and b["+i+"] aren't same Object."
			);
		} );
	},
	function(){
		cw( "Function: Jelp.new( String xml, ... )" );
		cw( "Try to create 3 new 'div' elements" );
		let a = _().new( "<div>", "<div>", "<div>" ); // Jelp 3 new divs
		let b = [ document.createElement( "div" ), document.createElement( "div" ), document.createElement( "div" ) ]; // JS equivalent
		cw( 'a = _().new( "<div>", "<div>", "<div>" )' );
		cl( a );
		cw( 'b = [ document.createElement( "div" ), document.createElement( "div" ), document.createElement( "div" ) ]' )
		cl( b );
		test(
			( a.length === b.length ),
			"Objects have same length",
			"Objects not have same length"
		);
		b.forEach( function( v, i ){
			test(
				( v.innerHTML === a[i].innerHTML ),
				"--> a["+i+"] and b["+i+"] have same 'innerHTML' property.",
				"--> a["+i+"] and b["+i+"] haven't same 'innerHTML' property"
			);
		} );
	},
	function(){
		cw( "Function: Jelp.remove()" );
		cw( "Try to remove all input" );
		let a = _( "input" );
		cw( 'a = _( "input" )' );
		cl( a );
		cw( 'Running a.remove()' );
		a.remove();
		let b = document.querySelectorAll( "input" );
		cw( "Getting removed elements, expected zero output." );
		cw( 'b = document.querySelectorAll( "input" )' );
		cl( b );
		test( ( b.length === 0 ), "b have legth = 0", "b NOT have legth = 0" );
	},
	function(){
		cw( "Benchmark: JelpEvent, how many times ?" );
		cw( "Expected globalEventCounter = 11" );
		test( ( globalEventCounter === 13 ), "globalEventCounter pass.", "globalEventCounter fail." );
	},
];

_( document ).ready( ()=>{
	console.log( "Test _( document ).ready() function." );
	let a = _("input");
	test( ( a.length === 4 ), "Document loaded correctly.", "Document NOT loaded correctly" );
});

let interval = setInterval( function(){
	if( enableTests ){
		if( typeof functionArray[globalCounter] === "function" ){
			if( globalCounter === 0 )
				cl( "Starting tests." );
			cg( functionArrayNames[globalCounter] );
			functionArray[globalCounter]();
			cG();
		}
		else{
			cl( "No more functions, end tests." );
			clearInterval( interval );
		}
		globalCounter++;
	}
}, 3000 );
