#!/usr/bin/env python

import http.server
import socketserver

with socketserver.TCPServer( ( "localhost", 8080 ), http.server.SimpleHTTPRequestHandler) as httpd:
    print("serving at port", httpd)
    httpd.serve_forever()
