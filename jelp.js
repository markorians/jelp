"use strict";

const _ = function(){
	return new class Jelp{
		constructor( args ){
			this.length = 0;
			document.dispatchEvent( this.JelpEvent() );
			document.JelpEvent = ( document.JelpEvent )?document.JelpEvent+1:1;
			try {
				for ( let a = 0; a < args.length ; a++ ) {
					let q = args[a];
					if( q.toString() === "Jelp" ){
						for( let i in q )
							if( !isNaN(i) )
								this[this.length++] = q[i];
					}
					else if( this.isHTMLNode( q ) ){
						this[this.length++] = q;
					}
					else if( q === document || q === window ){
						this[this.length++] = q;
					}
					else{
						let el = document.querySelectorAll( q );
						for(let e in el)
							if(!isNaN(e))
								this[this.length++] = el[e];
					}
				}
			}
			catch( e ){
				console.warning( "Jelp: " + e.name + ": " + e.message );
			}
		}
		JelpEvent(){
			return new Event( "JelpEvent" );
		}
		isHTMLNode( o ){
			return (
				typeof HTMLElement === "object" ? o instanceof HTMLElement : o &&
				typeof o==="object" &&
				o!==null &&
				o.nodeType===1 &&
				typeof o.nodeName==="string"
			);
		}
		ajax( settings ){
			let xhr = new XMLHttpRequest(), obj = settings || {};

			for( let o in obj )
				if( typeof obj[o] !== "undefined" && typeof xhr[o] !== "undefined" )
					xhr[o] = obj[o];

			[ "type", "url", "async", "data", "username", "password" ].forEach( function( v ){
				if( typeof obj[v] === "undefined" ){
					switch( v ){
						case "type":
							obj[v] = "GET";
						break;
						case "url":
							obj[v] = document.location.href;
						break;
						case "async":
							obj[v] = true;
						break;
						case "data":
						case "username":
						case "password":
							obj[v] = "";
						break;
					}
				}
			} );

			[ "success", "error", "complete", "progress", "beforeSend" ].forEach( function( v ){
				if( typeof obj[v] === "function" ){
					switch( v ){
						case "success":
							xhr.onload = function( ev ){
								if( ev.currentTarget.getResponseHeader('Content-Type').indexOf("json") !== -1 )
									obj[v]( JSON.parse( String( ev.currentTarget.response ) ) );
								else
									obj[v]( String( ev.currentTarget.response ) );
							};
						break;
						case "error":
							xhr.onerror = function( ev ){ obj[v]( ev.currentTarget ); };
						break;
						case "complete":
							xhr.onloadend = function( ev ){ obj[v]( ev.currentTarget ); };
						break;
						case "progress":
							xhr.onprogress = function( ev ){ obj[v]( ev.currentTarget ); };
						break;
						case "beforeSend":
							xhr.onloadstart = function( ev ){ obj[v]( ev.currentTarget ); };
						break;
					}
				}
			} );

			xhr.addEventListener( "loadend", function( ev ){
				if( ev.currentTarget.status != 200 && ev.currentTarget.onerror )
					ev.currentTarget.onerror( ev );
			} );

			xhr.open( obj.type, obj.url, obj.async, obj.username, obj.password );

			if( typeof obj["data"] === "string" )
				xhr.send( encodeURI( obj["data"] ) );
			else{
				let form = new FormData();
				for ( let k in obj["data"] ) {
					    form.append(k, obj["data"][k]);
				}
				xhr.send( form );
			}
			if( !this["XHRs"] )
				this["XHRs"] = [];

			this["XHRs"].push( xhr );
			return this;
		}
		new(){
			for ( let a = 0; a < arguments.length ; a++ ) {
				let x = document.createElement("x");
				if( typeof arguments[a] === "string" ){
					x.innerHTML = arguments[a];
					for( let i in x.childNodes )
						if( !isNaN(i) )
							this[this.length++] = x.childNodes[i];
				}
			}
			return this;
		}
		find( q ){
			try{
				if( q.toString() === "Jelp" ){
					if( q.length > 0 ){
						let arr = [];
						q.each( function( qq ){
							this.each( function( v, i ){
								if( qq === v ){
									arr.push( v );
								}
								else{
									let vect = v.querySelectorAll("*");
									vect.forEach( function( vv ){
										if( vv === qq )
											arr.push( vv );
									} );
								}
							} );
						} );
						for( let a in arr )
							if( !isNaN(a) )
								this[a] = arr[a];
						this.length = arr.length;
					}
				}
				else if( this.isHTMLNode( q ) ){
					let arr = [];
					this.each( function( v, i, o ){
						if( q === v ){
							arr.push( v );
						}
						else{
							let vect = v.querySelectorAll("*");
							vect.forEach( function( vv ){
								if( vv === q )
									arr.push( vv );
							} );
						}
						delete o[i];
					} );
					for( let a in arr )
						if( !isNaN(a) )
							this[a] = arr[a];
					this.length = arr.length;

				}
				else{
					let arr = [];
					this.each( function( v, i, o ){
						let a = v.querySelectorAll( q );
						if( a.length > 0 ){
							a.forEach( function( vv ){
								arr.push( vv );
							} );
						}
						delete o[i];
					} );
					for( let a in arr )
						if( !isNaN(a) )
							this[a] = arr[a];
					this.length = arr.length;
				}
			}
			catch( e ){
				console.warning( "Jelp: " + e.name + ": " + e.message );
			}
			return this;
		}
		show(){
			return this.css( "display", "initial" );
		}
		hide(){
			return this.css( "display", "none" );
		}
		prepend( el ){
			try{
				if( el.toString() === "Jelp" ){
					if( el.length > 0 )
						for( let e in el )
							if( !isNaN(e) )
								this.prepend( el[e] );
				}
				else if( this.isHTMLNode( el ) ){
					this.each(function( v ){
						v.prepend( el.cloneNode(true) );
					});
				}
				else{
					let x = document.createElement("x");
					if( typeof el === "string" )
						this.prepend( _().new( el ) );
				}
			}
			catch( e ){
				console.warning( "Jelp: " + e.name + ": " + e.message );
			}
			return this;
		}
		append( el ){
			try{
				if( el.toString() === "Jelp" ){
					if( el.length > 0 )
						for( let e in el )
							if( !isNaN(e) )
								this.append( el[e] );
				}
				else if( this.isHTMLNode( el ) ){
					this.each(function( v ){
						v.appendChild( el.cloneNode(true) );
					});
				}
				else{
					if( typeof el === "string" )
						this.append( _().new( el ) );
				}
			}
			catch( e ){
				console.warning( "Jelp: " + e.name + ": " + e.message );
			}
			return this;
		}
		ready( f ) {
			if( typeof f === "function" ){
				if (
					document.readyState === "complete" ||
					document.readyState === "interactive"
				) {
					setTimeout( f, 1 );
				} else {
					document.addEventListener( "DOMContentLoaded", f );
				}
			}
			return this;
		}
		click(){
			if( arguments.length === 1 ){
				if( typeof arguments[0] === "function" )
					this.on( "click", arguments[0] );
			}
			else
				this.fire( "click" );
			return this;
		}
		on( ev, f ){
			this.each( function( v ){
				try{
					v.addEventListener(ev, f);
				}
				catch( e ){
					console.warning( "Jelp: " + e.name + ": " + e.message );
				}
			});
			return this;
		}
		off( ev, f ){
			this.each( function( v, i, o ){
				if( ev === undefined && f === undefined ){
					let clone = v.cloneNode(true);
					v.parentNode.replaceChild( clone, v );
					o[i] = clone;
				}
				else{
					if( typeof v.removeEventListener !== "undefined" )
						v.removeEventListener( ev, f );
					else
						v.detachEvent( ev, f );
				}
			});
			return this;
		}
		fireJelp(){
			this.fire( "JelpEvent" );
			return this;
		}
		fire( t ){
			this.each( function( v ){
				if (v.fireEvent) {
					v.fireEvent( t );
				} else {
					let evObj = document.createEvent( "Events" );
					evObj.initEvent( t, true, false );
					v.dispatchEvent( evObj );
				}
			});
			return this;
		}
		css(){
			switch( arguments.length ){
				case 1:
					if( this.length > 0 ){
						if( typeof arguments[0] === "object" ){
							let obj = arguments[0];
							for( let o in obj ){
								this.css( o, obj[o] );
							}
						}
						if( typeof arguments[0] === "string" ){
							let styles = getComputedStyle( this[0] );
							if( styles.hasOwnProperty( arguments[0] ) ){
								return styles.getPropertyValue( arguments[0] )
							}
						}
					}
				break;
				case 2:
					let prop = arguments[0], val = arguments[1];
					this.each( function( v ){
						let styles = getComputedStyle( v );
						if( styles.hasOwnProperty( prop ) ){
							try {
								v.style.setProperty( prop, val );
							}
							catch( e ){
								console.warning( "Jelp: " + e.name + ": " + e.message );
							}
						}
					});
				break;
			}
			return this;
		}
		val(){
			return ( arguments.length > 0 )?this.prop( "value", arguments[0] ):this.prop( "value" );
		}
		text(){
			return ( arguments.length > 0 )?this.prop( "outerText", arguments[0] ):this.prop( "outerText" );
		}
		html(){
			return ( arguments.length > 0 )?this.prop( "outerHTML", arguments[0] ):this.prop( "outerHTML" );
		}
		prop(){
			switch( arguments.length ){
				case 1:
					if( this.length > 0 )
						if( typeof this[0][arguments[0]] !== "undefined" )
							return this[0][arguments[0]];
				break;
				case 2:
					let pr = arguments[0], vl = arguments[1];
					this.each( function( v ){
						if( typeof v[pr] !== "undefined" )
							v[pr] = vl;
					});
				break;
			}
			return this;
		}
		remove(){
			this.each( function( v, i, o ){
				try{
					v.parentNode.removeChild( v );
					delete o[i];
				}
				catch(e){
					console.warning( "Jelp: " + e.name + ": " + e.message );
				}
			});
			this.length = 0;
			return this;
		}
		each( f ){
			if( typeof f === "function" )
				for( let e in this )
					if( !isNaN(e) )
						f( this[e], e, this );
			return this;
		}
		toString(){ return "Jelp"; }
		valueOf(){ return this.length; }
	}( arguments );
}
